'use strict';

var Backbone = require('backbone');

var config = require('../my_app/config');
var models = require('./models');

exports.Resolutions = Backbone.Collection.extend({
  url: config.api + '/api/1/resolution/',
  model: models.Resolution
});
