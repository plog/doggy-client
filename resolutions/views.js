'use strict';

var Backbone = require('backbone');

var collections = require('./collections');
var itemTemplate = require('./templates/item');
var listTemplate = require('./templates/list');

exports.List = Backbone.View.extend({
  el: '#app-container',
  template: listTemplate,

  render: function() {
    this.$el.html(this.template());
    var collection = new collections.Resolutions();
    collection.fetch()
      .done(function(coll) {
        console.log(arguments)
        coll.forEach(function(model) {

          var view = new exports.Item({ model: model });
          return view.render();
        });
      });

    return this;
  },
});

exports.Item = Backbone.View.extend({
  el: '#resolution-list-container',
  template: itemTemplate,

  initialize: function(opts) {
    this.model = opts.model;
  },

  render: function() {
    this.$el.append(this.template(this.model));
    return this;
  },
});
