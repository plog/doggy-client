'use strict';

var Backbone = require('backbone');

var views = require('./views');

module.exports = Backbone.Router.extend({
  routes: {
    resolution: 'list',
  },
  list: function() {
    if (!window.localStorage.getItem('token')) {
      Backbone.history.navigate('/', { trigger: true });
    } else {
      var view = new views.List();
      return view.render();
    }
  },
});
