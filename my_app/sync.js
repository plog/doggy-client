'use strict';

var Backbone = require('backbone');

/*
 * Modifies Backbone.sync to include an Authorization header.
 * Also adds the image_dimensions as the query string parameter if
 * the model has such an attribute
 */
Backbone.originalSync = Backbone.sync;

Backbone.sync = function(method, model, opts) {
  var token = window.localStorage.getItem('token');
  if (token) {
    opts = opts || {};
    opts.headers = opts.headers || {};
    opts.headers.Authorization = 'JWT ' + token;
  }
  return Backbone.originalSync.call(this, method, model, opts);
};
