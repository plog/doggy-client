'use strict';

var Backbone = require('backbone');

var views = require('./views');

module.exports = Backbone.Router.extend({
  routes: {
    logout: 'logout',
    'sign-up': 'signUp',
    'sign-in': 'signIn'
  },
  logout: function() {
    window.localStorage.setItem('token', '');
    Backbone.history.navigate('/', { trigger: true });
  },
  signUp: function() {
    if (window.localStorage.getItem('token')) {
      Backbone.history.navigate('/resolution', { trigger: true });
    } else {
      var view = new views.SignUp();
      return view.render();
    }
  },
  signIn: function() {
    if (window.localStorage.getItem('token')) {
      Backbone.history.navigate('/resolution', { trigger: true });
    } else {
      var view = new views.SignIn();
      return view.render();
    }
  }
});
