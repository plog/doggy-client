/* global alert */
'use strict';

var $ = require('jquery');
var Backbone = require('backbone');

var models = require('./models');
var signInTemplate = require('./templates/sign_in');
var signUpTemplate = require('./templates/sign_up');

exports.SignUp = Backbone.View.extend({
  el: '#app-container',
  template: signUpTemplate,

  events: {
    submit: 'register',
  },

  render: function() {
    this.$el.html(this.template());
    return this;
  },

  register: function(evt) {
    evt.preventDefault();
    alert('to implement');
  }
});

exports.SignIn = Backbone.View.extend({
  el: '#app-container',
  template: signInTemplate,

  events: {
    submit: 'register',
  },

  render: function() {
    this.$el.html(this.template());
    return this;
  },

  register: function(evt) {
    evt.preventDefault();
    var data = {};
    var inputs = $(evt.currentTarget).find('input');
    inputs.each(function(ind, el) {
      el = $(el);
      var name = el.attr('name');
      if (name !== undefined) {
        data[name] = el.val();
      }
    });

    var user = new models.User();
    user.save(data)
      .done(function(model) {
        window.localStorage.setItem('token', model.token);
        Backbone.history.navigate('/resolution', { trigger: true });
      })
      .fail(function() {
        alert('bad login/password');
      });
  }
});
