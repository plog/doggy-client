'use strict';

var Backbone = require('backbone');

var config = require('../my_app/config');

exports.User = Backbone.Model.extend({
  urlRoot: config.api + '/login/',
});
