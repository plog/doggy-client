# Doggy client

## Frameworks and libs

- Backbone (MVC)
- i18next (i18n)
- handlebars (templating)
- borwserify (modularisation)
- lodash

## Installation

### Prerequisites:

* nodejs v0.10.x
* gulp v3.8.x (global)
* jshint v1.11.x (global)

Begin by installing [nodejs](http://nodejs.org/) and npm ([version to install](http://nodejs.org/dist/v0.8.15/node-v0.8.15.pkg)). You can use brew but you have to make sure you're installing the correct version.

    $ brew install node
    $ sudo npm install -g gulp@3.8.x

    $ cd <doggy_client>
    $ npm install

Run project

    # in an first terminal window
    $ cd <doggy_client>
    $ gulp dev

Open http://localhost:3000/ in your browser.

This task is serving the file and watching them. An autoreload mechanism is
triggered when a file has changed.

## Configuration

In `my_app/config.js`

## Coding Style

- line lenght: 80
- indent: 2 spaces (no tabs)
- coding style: [Felix's Node.js Style Guide](https://github.com/felixge/node-style-guide)
- see .jshintrc and .jscsrc for more specific rules

## Package management

### node packages

We use [npm](https://npmjs.org/). Packages are listed in `package.json` file and ignored by git. Be careful and run `npm install` to install new packages and **before deployment**.

## Tests and coverage

TODO: we would use

- mocha (test runner)
- chai (assertions)
- sinonjs (mock, spies, stubs)
- proxyquire (mock packages)
- istanbul (code coverage)
- karma (test driver)

## Deployment

### Setting up S3/Coudfront

Using S3 and cloudfront is an easy way to deploy a single page application.

*Warning:* Be careful at the bucket policy to be sure that the translation
files car be loaded correctly as they are JSON files (CORS are required).
