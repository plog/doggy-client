'use strict';

var Backbone = require('backbone');

var homeViews = require('./views');

module.exports = Backbone.Router.extend({
  routes: {
    '': 'home'
  },
  home: function() {
    if (window.localStorage.getItem('token')) {
      Backbone.history.navigate('/resolution', { trigger: true });
    } else {
      var view = new homeViews.Home();
      return view.render();
    }
  }
});
